<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>5-OurObject</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        </head>
    <body>
        <div class="container">
            <h1>5-OurObject</h1>

<?php
// require_once __DIR__ . '/../../private/CSTClasses_A/DbObject.php';
require_once __DIR__ . '/Autoloader.php';

$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_A",
        __DIR__ . '/../../private/CSTClasses_A' );

use CSTClasses_A\DbObject;

//Open a connection to MySQL (call mysqli_connect)
$db = new DbObject();
echo "<p>Successfully connected to the database!</p>\n";

// Query the database
// $qryResults = $db->runQuery( "SELECT firstName, lastName FROM Instructor" );
$qryResults = $db->select( "firstName, lastName", "Instructor" );

// Determine the number of rows returned and display it
$numRows = $qryResults->num_rows;
echo "<p>Fetched $numRows rows from the Instructor table</p>\n";

// instructorNum keeps track of which instructor number is being displayed
//$instructorNum = 0;

//echo "<div>\n";
//
//// LOOP while there are still instructors to be displayed
//while ( $instructor = $qryResults->fetch_array( MYSQLI_BOTH ) )
//// foreach ( $qryResults as $instructor )
//{
//    // Display the instructor information
//    $instructorNum++;
//    echo "Instructor $instructorNum: " .
//            "{$instructor[0]} {$instructor['lastName']}" .
//            "<br/>\n";
//}
//
////// LOOP while there are still instructors to be displayed
////for ( $instructorNum = 1; $instructorNum <= $numRows; $instructorNum++ )
////{
////    // Get the information for the next row
////    $instructor = $qryResults->fetch_row();
////    
////    // Display the instructor information
////    echo "Instructor $instructorNum: " .
////            "{$instructor[0]} {$instructor[1]}" .
////            "<br/>\n";
////}
//
//echo "</div>\n";

//echo "<p>The following fields were selected:</p>\n";
//echo "<ul>\n";
//
//// LOOP for all fields that were selected
//foreach ( $qryResults->fetch_fields() as $fieldInfo )
//{
//    // Display the name of the field
//    echo "<li>{$fieldInfo->name}</li>\n";
//}
//echo "</ul>\n";

// Display the records
DbObject::displayRecords( $qryResults );

// We're done with the results -- free them
$qryResults->free();

?>
            
        </div>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
