<?php
/**
 * Description of Autoloader
 *
 * @author ins208
 */
class Autoloader
{
    /**
     * http://www.php-fig.org/psr/psr-4/
     * PSR-4 defines behaviour for mapping FQ class names to files in PHP using the
     * php autoloader, namespaces and "use" directives.
     */
    
    //Tracks namespace prefixes, mapped to a set of directories containing classes within that namespace
    protected $prefixToBaseDirMappings = array();
    
    public function __construct()
    {
	$this->register();
    }
    
    public function register()
    {
	spl_autoload_register(array($this, 'loadClassByFullName'));
    }
    
    public function addNamespaceMapping($namespace, $basedir)
    {
	
	//Normalize namespace and basedir (neither should have trailing slash, only basedir can have leading)
	$namespace = trim($namespace, '\\'); 
	$basedir = rtrim($basedir, DIRECTORY_SEPARATOR);
	
	//Each namespace can have multiple base directories associated with it.
	// Make sure to initialize the array within prefixToBaseDir before
	// attempting to add to it
	if(!isset($this->prefixToBaseDirMappings[$namespace]))
	{
	    $this->prefixToBaseDirMappings[$namespace] = [];
	}
	
	$this->prefixToBaseDirMappings[$namespace][] = $basedir;
    }
    
    public function loadClassByFullName($class)
    {
	//We need to find the closest namespace match for the given class name.
	//We'll work backwards through the classname, looking for a namespace to 
	//basedir mapping which fits the given class name
	
	//Break the class name into namespace components. path\\to\\classname
	// would become array('path','to','classname');
	$namespacePathParts = explode('\\', $class);
	
	//Any parts of the class path which don't match a namespace are part of the
	// class path within the namespace
	$classpathParts = [];
	
	//Pass 1: look for a namespace mapping for \\path\\to\\class
	//  with class name of ''
	//Pass 2: look for a namespace mapping for \\path\\to
	//  with class name of class
	//Pass 3: look for a namespace mapping for \\path
	//  with class name of to/class
	//Pass 4: look for a namespace mapping for \\
	//  with class name of path/to/class
	//
	while(!empty($namespacePathParts))
	{
	    //Move one component from the namespace to the class path
	    array_unshift($classpathParts, array_pop($namespacePathParts));
	    //Re-create both the namespace and the class name
	    $namespace = implode('\\', $namespacePathParts);
	    $classpath = implode('/', $classpathParts);
	    
	    //If there is a mapping for the given namespace, attempt to load it
	    if(array_key_exists($namespace, $this->prefixToBaseDirMappings))
	    {
		//load file
		return $this->loadClassInNamespace( $classpath,$namespace);
	    }
	}
	return false;
    }
    
    /**
     * Given the namespace prefix and the path to the class within that namespace,
     * attempt to load the class from the set of registered basedirs in that 
     * namespace
     * @param type $classPath
     * @param type $prefix
     * @return boolean|string
     */
    private function loadClassInNamespace($classPath, $prefix)
    {
	foreach($this->prefixToBaseDirMappings[$prefix] as $baseDir)
	{
	    $filePath = $baseDir . '/' . $classPath . '.php';
	    if(file_exists($filePath))
	    {
		
		require_once $filePath;
		return $filePath;
	    }
	}
	return false;
    }
    

    
}


