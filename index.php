<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Learning Outcome 5</title>
    </head>
    <body>
        <h1>Learning Outcome 5 - Retrieving from a database</h1>
        <ol>
            <li><a href="5-Procedural.php">
                The procedural approach
                </a></li>
            <li><a href="5-Object.php">
                The Object-oriented approach
                </a></li>
            <li><a href="5-OurObject.php">
                The Object-oriented approach with our own object
                </a></li>
        </ol>
    </body>
</html>